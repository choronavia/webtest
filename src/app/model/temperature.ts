import { Daily } from './daily';
import { Hourly } from './hourly';
import { Minutely } from './minutely';
import { Currently } from './currently';
import {Flags} from './flags'

export interface Temperature {
    latitude: number;
    longitude: number;
    timezone: string;
    currently: Currently;
    minutely: Minutely;
    hourly: Hourly;
    daily: Daily;
    flags: Flags;
    offset: number;
}
