import { Data } from "./data";
export interface Minutely {
    summary: string;
    icon: string;
    data: Data[];
}
