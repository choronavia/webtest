import { DataDaily } from "./dataDaily";
export interface Daily {
    summary: string;
    icon: string;
    data: DataDaily[];
}
