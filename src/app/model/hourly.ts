import { Data } from "./data";
export interface Hourly {
    summary: string;
    icon: string;
    data: Data[];
}
