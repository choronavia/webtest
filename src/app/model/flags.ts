export interface Flags {
    sources: string[];
    nearestStation: number;
    units: string;
}