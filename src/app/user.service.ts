import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Temperature } from './model/temperature';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TemperatureService {

  constructor(protected http: HttpClient) { }

  getTemperature(coordenate: string):Observable<Temperature> {
    console.log(coordenate);
    let URL = 'http://localhost:3001/place/' + coordenate;
    return this.http.get<Temperature>(URL);
  }
  
}
