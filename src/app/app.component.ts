import { Component } from '@angular/core';
import { TemperatureService } from './user.service';
import {Temperature} from './model/temperature'
import {Zurich} from './model/zurich'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'webTest';

  temperature: Temperature;
  zurich: Zurich;
  

  constructor(
    protected temperatureService: TemperatureService
  ) {
  }

  ngOnInit() {
    
    this.temperatureService.getTemperature('-33.4372,-70.6506')
    .subscribe(
      (data) => { // Success
        console.log(data);
        this.temperature = data;
      },
      (error) => {
        console.error(error);
      }
    );

    this.temperatureService.getTemperature('47.3666700,8.5500000')
    .subscribe(
      (data) => {
        console.log('2: '+data);
        this.zurich.temperature = data;
      },
      (error) => {
        console.error(error);
      }
      );
  }
  
}
