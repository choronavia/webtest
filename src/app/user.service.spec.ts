import { TestBed } from '@angular/core/testing';

import { TemperatureService } from './user.service';

describe('UserService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TemperatureService = TestBed.get(TemperatureService);
    expect(service).toBeTruthy();
  });
});
